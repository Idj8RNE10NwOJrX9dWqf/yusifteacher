package com.example.yusifteacher.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import android.util.Log;
import android.widget.Toast;

import com.example.yusifteacher.utils.GetContactName;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.Arrays;

import cz.msebera.android.httpclient.Header;


public class CallChecker extends BroadcastReceiver {
    private static final String TAG = "CallChecker";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)) {
                String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                try {
                    Toast.makeText(context, GetContactName.getContactName(context, number), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context, number, Toast.LENGTH_SHORT).show();
                }
            }
            if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                Toast.makeText(context, "Received", Toast.LENGTH_SHORT).show();
            }
            if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_IDLE)) {
                endCall();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void endCall() {
        AsyncHttpClient client = new AsyncHttpClient();
        String url = String.format("http://192.168.43.135:8000/query/?name=%s&number=%s", "?", "?");
        Log.d(TAG, "send: " + url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                System.out.println(TAG + ": " + Arrays.toString(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                System.out.println(TAG + ": " + error.getMessage());
            }
        });
    }
}