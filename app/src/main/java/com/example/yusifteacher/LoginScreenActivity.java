package com.example.yusifteacher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.os.Bundle;
import android.text.style.UnderlineSpan;
import android.view.View;

import com.example.yusifteacher.databinding.ActivityLoginScreenBinding;
/*
* Hello world 123*/
public class LoginScreenActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityLoginScreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login_screen);

        binding.registration.setPaintFlags(binding.registration.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        binding.login.setOnClickListener(this);
        binding.registration.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login:
                performLogin();
                break;
            case R.id.registration:
                goToRegistration();
                break;
        }
    }

    private void performLogin(){
        Intent intent = new Intent(LoginScreenActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void goToRegistration(){
        Intent intent = new Intent(LoginScreenActivity.this, RegistrationActivity.class);
        startActivity(intent);
    }
}
