package com.example.yusifteacher.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.Arrays;

import cz.msebera.android.httpclient.Header;

public class GetContactName {
    private static final String TAG = "GetContactName";

    public static String getContactName(Context context, String number) {

        String name = null;

        String[] projection = new String[] {
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID};

        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);

        if(cursor != null) {
            if (cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                Log.v(TAG, "Started: Contact number = " + number);
                Log.v(TAG, "Started: Contact name  = " + name);
                send(name, number);
            } else {
                Log.v(TAG, "Contact Not Found = " + number);
                send("Unknown", number);
            }
            cursor.close();
        }
        return name;
    }

    private static void send(String name, String phoneNumber){
        AsyncHttpClient client = new AsyncHttpClient();
        String url = String.format("http://192.168.43.135:8000/query/?name=%s&number=%s", name, phoneNumber);
        Log.d(TAG, "send: " + url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                System.out.println(TAG + ": " + Arrays.toString(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                System.out.println(TAG + ": " + error.getMessage());
            }
        });
    }
}