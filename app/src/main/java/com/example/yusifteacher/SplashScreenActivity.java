package com.example.yusifteacher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreenActivity extends AppCompatActivity {

    private enum UserState{
        loggedIn,
        signedOut
    }

    private static final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        UserState userState = getUserState();

        splash(userState);
    }

    private void goToLoginScreen(){
        Intent loginIntent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
        startActivity(loginIntent);
        finish();
    }

    private void goToMainScreen(){
        Intent mainIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(mainIntent);
        finish();
    }

    private void splash(final UserState userState){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (userState){
                    case loggedIn:
                        goToMainScreen();
                    case signedOut:
                        goToLoginScreen();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private UserState getUserState(){
        return UserState.signedOut;
    }
}

